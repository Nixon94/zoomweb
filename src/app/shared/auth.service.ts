import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private static readonly tokenKey = 'access_token';
  private isLongLiving: boolean;

  get storage() {
    return this.isLongLiving ? localStorage : sessionStorage;
  }

  setAccessToken(token: string, isLongLiving: boolean): void {
    this.isLongLiving = isLongLiving;
    this.storage.setItem(AuthService.tokenKey, token);
  }

  getAccessToken(): string {
    let token = this.storage.getItem(AuthService.tokenKey);
    if (!token) {
      this.isLongLiving = !this.isLongLiving;
      token = this.storage.getItem(AuthService.tokenKey);
    }
    return token;
  }
}
