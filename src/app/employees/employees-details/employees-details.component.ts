import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../shared/employee.model';
import { EmployeesService } from '../shared/employees.service';


@Component({
  selector: 'app-employees-details',
  templateUrl: './employees-details.component.html',
  styleUrls: ['./employees-details.component.css']
})
export class EmployeesDetailsComponent implements OnInit {

  employeeId: string;
  employeeAvatar: string;
  employeeName: string;
  employeePosition: string;
  employee: Employee;
  positions: string[] = ['', 'Młodszy_Weterynarz', 'Starszy_Weterynarz', 'Opiekun', 'Grabarz'];
  selectedType: string;
  employeeImage: string;
  updatable: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly service: EmployeesService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employeeId = params.id;
      this.getEmployee();
      this.selectedType = '';
      this.employeeImage = '';
    });
  }

  async getEmployee() {
    this.employee = await this.service.download(this.employeeId).toPromise();
    this.employeeAvatar = this.employee.avatar;
    this.employeePosition = this.employee.position;
  }

  navigateToEmployeeList() {
    this.router.navigate(['/employees']);
  }

  async updateEmployee() {
    if (!this.isPositionEmpty()) {
      this.employee.position = this.selectedType;
    }
    
    this.employee.avatar = this.employeeImage;
    await this.service.update(this.employee).toPromise();
    this.navigateToEmployeeList();
  }

  isImageEmpty(): boolean {
    return (this.employeeImage === '' );
  }
  isPositionEmpty(): boolean {
    return (this.selectedType === '');
  }
  cantUpdate(): boolean {
  return (this.isImageEmpty() && this.isPositionEmpty());
  }

}
