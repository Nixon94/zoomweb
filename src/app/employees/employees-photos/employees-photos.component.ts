import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { Employee } from '../shared/employee.model';

@Component({
  selector: 'app-employees-photos',
  templateUrl: './employees-photos.component.html',
  styleUrls: ['./employees-photos.component.css']
})
export class EmployeesPhotosComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'background-color: grey';
  @Input() imageUrl: string;
  @Input() employee: Employee;
  @Output() employeeSelected: EventEmitter<Employee>;

  constructor() {
    this.employeeSelected = new EventEmitter<Employee>();
  }

  ngOnInit() {
  }

  createPath(): string {
    return this.imageUrl || '/assets/images/default_avatar.jpg';
  }

  clicked() {
    this.employeeSelected.emit(this.employee);
  }
}
