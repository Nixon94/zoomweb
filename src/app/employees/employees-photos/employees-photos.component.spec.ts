import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesPhotosComponent } from './employees-photos.component';

describe('EmployeesPhotosComponent', () => {
  let component: EmployeesPhotosComponent;
  let fixture: ComponentFixture<EmployeesPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
