import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { EmployeesService } from '../shared/employees.service';
import { Employee } from '../shared/employee.model';
import { fromEvent, of } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';
import { getLocaleDateTimeFormat } from '@angular/common';


@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  constructor(private readonly service: EmployeesService, private elementRef: ElementRef) { }

  readonly maxYear = new Date().getFullYear() - 18;
  readonly minYear = new Date().getFullYear() - 80;
  employees: Employee[];
  employee: Employee = new Employee();
  public selectedName: string;
  params = '';
  searchBox: HTMLInputElement;
  title: string;
  positions: string[] = ['', 'Młodszy_Weterynarz', 'Starszy_Weterynarz', 'Opiekun', 'Grabarz'];
  newEmployeeName: string;
  newEmployeePosition: string;
  newEmployeeYOB: number;


  ngOnInit() {
    this.searchEmployees();
    this.newEmployeeName = '';
    this.newEmployeePosition = '';
    this.newEmployeeYOB = 2019;
  }

  getAge(employee: Employee) {
    return 2019 - employee.yearOfBirth;
  }
  searchEmployees(): void {
    this.service.search(`?Name=${this.params}`)
      .subscribe(employees => this.employees = employees);
  }

  getEmployees$() {
    return this.service.search(`?Name=${this.params}`);
  }

  ngAfterViewInit(): void {
    this.searchBox = document.getElementById('search-box') as HTMLInputElement;
    fromEvent(this.searchBox, 'input').pipe(
      map((e: KeyboardEvent) => { const target = e.target as HTMLInputElement; return target.value; }),
      distinctUntilChanged(),
      switchMap(() => this.getEmployees$()
        .pipe(catchError(() => of(this.employees)))
      )
    ).subscribe(ee => this.employees = ee,
      e => console.log(e));
  }

  public highlightRow(employee) {
    this.selectedName = employee.id;
  }

  public employeeWasSelected(employee: Employee) {
    console.log(employee);
  }

  async deleteEmployee(employee: Employee) {
    const isConfirmed = confirm('Are you sure?');
    if (!isConfirmed) {
      return;
    }
    await this.service.remove(employee).toPromise();
    await this.searchEmployees();
  }

  async addEmployee() {
    if (!this.cantUpdate() || !this.isYearWrong()) {
      this.employee.name = this.newEmployeeName;
      this.employee.position = this.newEmployeePosition;
      this.employee.yearOfBirth = this.newEmployeeYOB;
      this.employee.avatar = 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png';

      await this.service.add(this.employee).toPromise();

      this.employee = new Employee();
      await this.searchEmployees();
    }
    this.newEmployeeName = '';
    this.newEmployeePosition = '';
  }

  isNameempty(): boolean {
    return (this.newEmployeeName === '');
  }
  isPositionEmpty(): boolean {
    return (this.newEmployeePosition === '');
  }

  isYearWrong(): boolean {
    return (this.newEmployeeYOB > this.maxYear || this.newEmployeeYOB < this.minYear);
  }
  cantUpdate(): boolean {
    return (this.isNameempty() || this.isPositionEmpty());
  }
}
