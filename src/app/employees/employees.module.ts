import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeesPhotosComponent } from './employees-photos/employees-photos.component';
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { EmployeesDetailsComponent } from './employees-details/employees-details.component';


@NgModule({
  declarations: [EmployeesPhotosComponent, EmployeesListComponent, EmployeesDetailsComponent],
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class EmployeesModule { }
