export class Employee {
  id: number;
  name: string;
  position: string;
  avatar: string;
  yearOfBirth: number;
}
