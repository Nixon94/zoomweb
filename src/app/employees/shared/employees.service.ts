import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../shared/auth.service';
import { environment } from '../../../environments/environment';
import { Employee } from './employee.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService extends ApiService {

  constructor(
    httpClient: HttpClient,
    authService: AuthService) {
    super(
      environment.apiUrl,
      httpClient,
      authService);
  }

  search(match?: string): Observable<Employee[]> {
    return super.get<Employee[]>(match, true);
  }

  download(id: string): Observable<Employee> {
    return super.get<Employee>(id, true);
  }

  add(employee: Employee): Observable<any> {
    return super.post('', employee, true);
  }

  remove(employee: Employee): Observable<any> {
    return super.delete<Employee>(`${employee.id}`, true);
  }

  update(employee: Employee): Observable<any> {
    return super.put(`${employee.id}`, employee, true);
  }

}
