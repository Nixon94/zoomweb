import { Component } from '@angular/core';
import { AuthService } from './shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ZooM';
  token = 'access_token';
  route: string;
  constructor(authService: AuthService, private router: Router ) {
    authService.setAccessToken(this.token, true);
    router.events.subscribe(() => this.route = router.url);
}
}
